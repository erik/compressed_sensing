#include <algorithm>
#include <Eigen/Core>
#include <iostream>
#include <random>
#include <vector>

//--------------------------------------------------------------------------------------------------
using Scalar = double;
using Vector = Eigen::Matrix<Scalar, Eigen::Dynamic, 1>;
using Matrix = Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>;

//--------------------------------------------------------------------------------------------------
constexpr Scalar pi = 3.1415926535897932384626433832795;

//--------------------------------------------------------------------------------------------------
void python_print(char const* name, Vector const& x) {
    std::cout << name << " = np.array([";
    std::cout << x[0];
    for (uint32_t i = 1; i < x.size(); ++i) {
        std::cout << ", " << x[i];
    }
    std::cout << "])\n";
}

//--------------------------------------------------------------------------------------------------
Vector sample_two_sins(Scalar f1, Scalar f2, Vector const& sample_times) {
    Vector const sample_rads = 2 * pi * sample_times;
    return sin((f1 * sample_rads).array()) + sin((f2 * sample_rads).array());
}

//--------------------------------------------------------------------------------------------------
Matrix compute_dct_matrix(uint32_t n_samples) {
    Matrix dct_matrix(n_samples, n_samples);
    Scalar root_one_over_n = std::sqrt(1.0 / n_samples);
    Scalar root_two_over_n = std::sqrt(2.0 / n_samples);
    for (uint32_t j = 0; j < n_samples; ++j) {
        dct_matrix(0, j) = root_one_over_n;
    }
    for (uint32_t i = 1; i < n_samples; ++i) {
        for (uint32_t j = 0; j < n_samples; ++j) {
            dct_matrix(i, j) = root_two_over_n * cos(pi * (2 * j + 1) * i / (2 * n_samples));
        }
    }
    return dct_matrix;
}

//--------------------------------------------------------------------------------------------------
std::vector<uint32_t> select_subsample(uint32_t n_samples, uint32_t n_subsamples) {
    std::vector<uint32_t> subset_indices;
    subset_indices.resize(n_samples);
    for (uint32_t i = 0; i < n_samples; ++i) {
        subset_indices[i] = i;
    }
    auto rng = std::default_random_engine();
    rng.seed(std::default_random_engine::result_type(84610373847));
    std::shuffle(std::begin(subset_indices), std::end(subset_indices), rng);
    subset_indices.resize(n_subsamples);
    std::sort(subset_indices.begin(), subset_indices.end());
    return subset_indices;
}

//--------------------------------------------------------------------------------------------------
// Selects certain elements of a Vector.
Vector vector_subset(Vector const& vector, std::vector<uint32_t> const& subset_indices) {
    Vector subset(subset_indices.size());
    for (uint32_t i = 0; i < subset_indices.size(); ++i) {
        auto const index = subset_indices[i];
        subset[i] = vector[index];
    }
    return subset;
}

//--------------------------------------------------------------------------------------------------
// Selects certain columns of a Matrix.
Matrix matrix_subset(Matrix const& matrix, std::vector<uint32_t> const& subset_indices) {
    Matrix subset(matrix.rows(), subset_indices.size());
    for (uint32_t i = 0; i < subset_indices.size(); ++i) {
        auto const index = subset_indices[i];
        subset.col(i) = matrix.col(index);
    }
    return subset;
}

//--------------------------------------------------------------------------------------------------
int main() {
    constexpr Scalar f1 = 697;
    constexpr Scalar f2 = 1209;

    // Part (a)
    constexpr Scalar sample_period = 0.01;
    constexpr uint32_t n_samples = 250;
    Vector const sample_times = (sample_period / n_samples) * Vector::LinSpaced(n_samples, 0, n_samples);
    Vector const sample_values = sample_two_sins(f1, f2, sample_times);
    python_print("sample_times", sample_times);
    python_print("sample_values", sample_values);

    // Part (b)
    Matrix const dct_matrix = compute_dct_matrix(n_samples);
    Vector const dct = dct_matrix * sample_values;
    python_print("dct", dct);

    // Part (c)
    Vector recovered_sample_values = dct_matrix.transpose() * dct;
    python_print("recovered_sample_values", recovered_sample_values);

    // Part (d)
    constexpr uint32_t n_subsamples = 100;
    std::vector<uint32_t> const subset_indices = select_subsample(n_samples, n_subsamples);
    Vector subset_sample_times = vector_subset(sample_times, subset_indices);
    Vector subset_sample_values = vector_subset(sample_values, subset_indices);
    python_print("subset_sample_times", subset_sample_times);
    python_print("subset_sample_values", subset_sample_values);

    // Part (e)
    Matrix const subset_dct_matrix = matrix_subset(dct_matrix, subset_indices);
    Vector recovered_dct = Vector::Random(n_samples);
    recovered_sample_values = dct_matrix.transpose() * recovered_dct;
    Vector subset_recovered_sample_values = vector_subset(recovered_sample_values, subset_indices);
    Vector subset_differences = subset_sample_values - subset_recovered_sample_values;
    Scalar loss = subset_differences.squaredNorm();
    Vector gradient = -2 * subset_dct_matrix * subset_differences;
    Scalar learning_rate = 0.1;
    uint32_t count = 0;
    while (loss > 1e-6) {
        recovered_dct -= learning_rate * gradient;
        recovered_sample_values = dct_matrix.transpose() * recovered_dct;
        subset_recovered_sample_values = vector_subset(recovered_sample_values, subset_indices);
        subset_differences = subset_sample_values - subset_recovered_sample_values;
        loss = subset_differences.squaredNorm();
        gradient = -2 * subset_dct_matrix * subset_differences;
        //std::cout << loss << '\n';
        ++count;
    }
    Scalar const final_loss_e = loss;
    uint32_t const final_count_e = count;
    python_print("dct_e", recovered_dct);
    python_print("recovered_sample_values_e", recovered_sample_values);

    // Part (f)
    recovered_dct = Vector::Random(n_samples);
    recovered_sample_values = dct_matrix.transpose() * recovered_dct;
    subset_recovered_sample_values = vector_subset(recovered_sample_values, subset_indices);
    subset_differences = subset_sample_values - subset_recovered_sample_values;
    loss = subset_differences.squaredNorm() + recovered_dct.squaredNorm();
    gradient = -2 * subset_dct_matrix * subset_differences + 2 * recovered_dct;
    learning_rate = 0.1;
    count = 0;
    while (gradient.squaredNorm() > 1e-6) {
        recovered_dct -= learning_rate * gradient;
        recovered_sample_values = dct_matrix.transpose() * recovered_dct;
        subset_recovered_sample_values = vector_subset(recovered_sample_values, subset_indices);
        subset_differences = subset_sample_values - subset_recovered_sample_values;
        loss = subset_differences.squaredNorm() + recovered_dct.squaredNorm();
        gradient = -2 * subset_dct_matrix * subset_differences + 2 * recovered_dct;
        if (count % 32 == 0) {
            learning_rate *= 0.99;
            /*
            std::cout << "loss: " << loss;
            std::cout << ", learning rate: " << learning_rate;
            std::cout << ", |grad|^2: " << gradient.squaredNorm();
            std::cout << '\n';
            */
        }
        if (count > 100000) {
            break;
        }
        ++count;
    }
    Scalar const final_loss_f = loss;
    uint32_t const final_count_f = count;
    python_print("dct_f", recovered_dct);
    python_print("recovered_sample_values_f", recovered_sample_values);

    // Part (g)
    recovered_dct = Vector::Random(n_samples);
    recovered_sample_values = dct_matrix.transpose() * recovered_dct;
    subset_recovered_sample_values = vector_subset(recovered_sample_values, subset_indices);
    subset_differences = subset_sample_values - subset_recovered_sample_values;
    loss = subset_differences.squaredNorm() + recovered_dct.cwiseAbs().sum();
    gradient = -2 * subset_dct_matrix * subset_differences;
    for (uint32_t i = 0; i < n_samples; ++i) {
        if (std::abs(recovered_dct[i]) > 1e-3) {
            gradient[i] += (recovered_dct[i] > 0) ? 1 : -1;
        }
    }
    Scalar last_loss = std::numeric_limits<Scalar>::infinity();
    Scalar relative_change = std::abs(loss - last_loss) / loss;
    learning_rate = 0.1;
    count = 0;
    while (relative_change > 1e-9) {
        recovered_dct -= learning_rate * gradient;
        recovered_sample_values = dct_matrix.transpose() * recovered_dct;
        subset_recovered_sample_values = vector_subset(recovered_sample_values, subset_indices);
        subset_differences = subset_sample_values - subset_recovered_sample_values;
        last_loss = loss;
        loss = subset_differences.squaredNorm() + recovered_dct.cwiseAbs().sum();
        gradient = -2 * subset_dct_matrix * subset_differences;
        for (uint32_t i = 0; i < n_samples; ++i) {
            if (std::abs(recovered_dct[i]) > 1e-3) {
                gradient[i] += (recovered_dct[i] > 0) ? 1 : -1;
            }
        }
        relative_change = std::abs(loss - last_loss) / loss;
        if (count % 64 == 0) {
            learning_rate *= 0.99;
            /*
            std::cout << "loss: " << loss;
            std::cout << ", learning rate: " << learning_rate;
            std::cout << ", |grad|^2: " << gradient.squaredNorm();
            std::cout << '\n';
            */
        }
        if (count > 500000) {
            break;
        }
        ++count;
    }
    //std::cout << "g iterations: " << count;
    //std::cout << "g squared gradient norm: " << gradient.squaredNorm();
    Scalar const final_loss_g = loss;
    uint32_t const final_count_g = count;
    python_print("dct_g", recovered_dct);
    python_print("recovered_sample_values_g", recovered_sample_values);

    std::cout << '\n';
    std::cout << "Final unregularized loss: " << final_loss_e
              << " (" << final_count_e << " iterations)\n";
    std::cout << "Final L2 regularized loss: " << final_loss_f
              << " (" << final_count_f << " iterations)\n";
    std::cout << "Final L1 regularized loss: " << final_loss_g
              << " (" << final_count_g << " iterations)\n";

    return 0;
}
