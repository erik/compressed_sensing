# Compressed Sensing

I used this code to solve problem 15.7 of Neil's Physics of Information
Technology, as taught spring 2019. For now you can access the class page
[here](http://fab.cba.mit.edu/classes/862.19/), and my solutions for this
particular problem set
[here](http://fab.cba.mit.edu/classes/862.19/people/erik/psets/12.html), though
these links may
change in the future and I don't promise to keep this up to date.

## Dependencies

The C++ code is built with cmake, and uses Eigen. It prints numpy arrays to
stdout that you can paste into `plotter.py` (sorry). The plotting code uses
numpy and matplotlib. I use Python 3 but it would probably work with Python 2 as
well.

## Building

First clone the repo, or download and extract all files. Then from the project's
root directory, run

```
mkdir build
cd build
cmake ..
make
```

An executable `compressed_sensing` will be generated in the `build` directory.