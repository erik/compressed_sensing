# This file defines an interface library used to add common compile flags to all targets.

add_library(shared_settings INTERFACE)

# Warning flags
target_compile_options(shared_settings INTERFACE
    -Wall
    -Wcast-align
    -Wcast-qual
    -Wextra
    -Wundef
    -Wuseless-cast
    -Wzero-as-null-pointer-constant
    -pedantic
)

# Speed flags
target_compile_options(shared_settings INTERFACE -march=native -ffast-math)

# Build type for profile generation
target_compile_options(shared_settings INTERFACE $<$<CONFIG:ProfileGenerate>:
    -fprofile-generate
    -O3
    -DNDEBUG
>)
target_link_options(shared_settings INTERFACE $<$<CONFIG:ProfileGenerate>:-fprofile-generate>)

# Build type for profile use
target_compile_options(shared_settings INTERFACE $<$<CONFIG:ProfileUse>:
    -fprofile-use
    -O3
    -DNDEBUG
>)
target_link_options(shared_settings INTERFACE $<$<CONFIG:ProfileUse>:-fprofile-use>)
